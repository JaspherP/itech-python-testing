import unittest

# Erstellen Sie für die folgenden Methoden Tests.
# Nutzen Sie die Klasse TestBeispieleTestase für die erstellten Tests.
# Die Tests können über die Konsole mit python <Name_der_Datei>.py ausgeführt werden.
# Viel Spaß :-)!


def dummy():
    return "dummy"

def multiplikation(zahl_eins, zahl_zwei):
    ergebnis = zahl_eins * zahl_zwei
    return ergebnis

def string_verkettung (string_eins, string_zwei):
    ergebnis = string_eins + string_zwei
    return ergebnis

def rueckgabe_einer_liste_ohne_duplikate (liste):
    liste_ohne_duplikate = set(liste)
    return liste_ohne_duplikate

def rueckgabe_index_und_wert_maximum(liste):
    maxWert = liste[0]
    index = 0
    for i, wert in enumerate(liste):
        if(wert > maxWert):
            maxWert = wert
            index  = i
    return [index, maxWert]

def vergleiche_ob_integer_groesser_ist(zahl_eins, zahl_zwei):
    ergebnis = zahl_eins > zahl_zwei
    return ergebnis


class TestBeispieleTestCase(unittest.TestCase):
    def test_multiplikation(self):
        ergebnis = dummy()
        self.assertEqual("dummy", ergebnis)

    def test_multiplikation(self):
        ergebnis = multiplikation(5, 7)
        self.assertEqual(35, ergebnis)

    def test_string_verkettung(self):
        ergebnis = string_verkettung("Hallo", " ITECH")
        self.assertEqual("Hallo ITECH", ergebnis)

    def test_rueckgabe_einer_liste_ohne_duplikate(self):
        ergebnis = rueckgabe_einer_liste_ohne_duplikate({1, 2, 2, 3, 3, 3, 4, 4, 4, 5, 5, 5, 5 ,5})
        eindeutige_liste = {1, 2, 3, 4, 5}
        print ("Vergleich von Ergebnis {} und {} ".format(ergebnis, eindeutige_liste))
        self.assertEqual(ergebnis, eindeutige_liste)

    def test_rueckgabe_index_und_wert_maximum(self):
        ergebnis = rueckgabe_index_und_wert_maximum([0, -1, 3, 1, -5, -1])
        self.assertEqual([2, 3], ergebnis)

    def test_vergleiche_ob_integer_groesser_ist(self):
        ergebnis = vergleiche_ob_integer_groesser_ist(7, 5)
        self.assertTrue(ergebnis)

# Verbosity=2 sorgt dafür, dass die Ausgaben "wortreichener" sind.
# Wenn Sie den Level auf 1 ändern, erkennen Sie den Unterschied.
if __name__=='__main__':
    unittest.main(verbosity=2)

